export const isToday = (someDate: any) => {
    const today = new Date();
    return someDate.getDate() === today.getDate() &&
      someDate.getMonth() === today.getMonth() &&
      someDate.getFullYear() === today.getFullYear()
}

export const isSameYear = (someDate: any) => {
    const today = new Date();
    return someDate.getFullYear() === today.getFullYear()
}

export const formatDateTime = (someDate: any, hourDisplay: string) => {
    const formatYear = !isSameYear(someDate) ? '/YYYY' : '';
    const formatDate = !isToday(someDate) ? `MM/DD${formatYear} ` : '';
    const formatTime = hourDisplay === 'HH' ? 'HH:mm' : 'hh:mm a';

    return `${formatDate}${formatTime}`;
}

export const secondsDifference = (date1: string, date2: string) => {
    const t1 = new Date(date1);
    const t2 = new Date(date2);
    const dif = t1.getTime() - t2.getTime();
    
    return dif / 1000;
}
