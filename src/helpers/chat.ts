export const constructMessage = (payload: any, isGuest: boolean) => ({
    message: payload.value,
    dateTime: new Date(),
    isGuest,
    userName: payload.userName || ''
});
