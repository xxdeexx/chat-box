import { LocalizeContextProps } from 'react-localize-redux';

export enum THEME_TYPE {
    LIGHT = 'light',
    DARK = 'dark'
}

export interface IMessage {
	userName?: string;
	dateTime: any;
	isGuest: boolean;
	message: string;
}

export interface IState {
	readonly userName: string;
	readonly themeColor: THEME_TYPE;
	readonly hourDisplay: string;
	readonly sendMessageControl: boolean;
	readonly language: string;
	readonly messages: IMessage[];
}

export interface IAllState {
	app: IState
}

export interface IAppProps extends LocalizeContextProps {
	themeColor: THEME_TYPE;
}

export interface CALLBACK {
    name: string;
    value: string | number | boolean;
}

export interface ISettingsProps extends LocalizeContextProps {
    userName: string;
    themeColor: string;
    hourDisplay: string;
    sendMessageControl: boolean;
    settingsFieldChangeAction: (payload: CALLBACK) => void;
    settingsResetAction: () => void;
}

export interface IHeaderProps {
    toggleSettings: () => void
}

export interface ICommonFieldProps {
    name: string;
    id: string;
    label?: any;
    value?: string | number;
    callback?: (payload: CALLBACK) => void;
    options?: any[];
}

export interface ITextFieldProps extends ICommonFieldProps {
    multiline?: boolean;
    callbackOnSubmit?: boolean;
    callbackOnCtrlSubmit?: boolean;
    callbackOnBlur?: boolean;
    endAdornment?: any;
}

export interface IChatInputsProps {
    sendMessageControl: boolean;
    sendWsMessageAction: (payload: CALLBACK) => void;
}
