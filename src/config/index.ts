export * from './locale';

export const USER_TYPE = {
    ADMIN: 'ADMIN',
    MANAGER: 'MANAGER',
    USER: 'USER'
}

export const CHAT_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRjYTY1NGQyMTMzNzUyZjUzMmU4ZmQzNWRlZjJkYTg3ZmMzMGJiNjEyNTcyMDVhZGRkNzQzYzZmMjI4NWIzOGQwM2MzZjJlYzYyOGU3Y2VjIn0.eyJhdWQiOiI4IiwianRpIjoiNGNhNjU0ZDIxMzM3NTJmNTMyZThmZDM1ZGVmMmRhODdmYzMwYmI2MTI1NzIwNWFkZGQ3NDNjNmYyMjg1YjM4ZDAzYzNmMmVjNjI4ZTdjZWMiLCJpYXQiOjE1ODQzNjk3NzcsIm5iZiI6MTU4NDM2OTc3NywiZXhwIjoxNjE1OTA1Nzc3LCJzdWIiOiI2NzciLCJzY29wZXMiOltdfQ.nIeEEdHAgCHnCg6RPreN-CxTVml2CL_7WBq2k4eT5zK--BeXeD_amL68oE9dOE4wHsK3s-uhj5W8-QUTyV39U0KWvRf8KshQFmf4lJJ0LEV2DHxGBERoE6tLTm8sDXrxbYbeLH3r41XKKi9sr4My2kTwmjf5g3r0yGj4Wt8mnBsA-KmPSSzDY3nowupijMpd0f3DaWXPvRadswLycSX31nIWp6NvHYoHtpXvSzRKRcAcuJxnA9nab9pFbKG5BTbW7hWMO9w7DEYPlZYp4pxHYQoms8_jX9BBDYfQ-jBQ7LJDliivpizrzSMr8uf_Hf8cqgOQvTyWCgdDeOaRoJfcvPsshDVNZZAztkXoh4sIz-TzbAMfg2uF83locWzi71WcCeGOy9cha7GWb7X1q-FgjGC6ScMc80dX_aAog-K7FmsFrl2Yws1B2e5NF98K6xT8uAueAxoCzWUAs9thjR4fbKZ8lA-M-Rw3p0gdj7Y8y21nhEPQqq0QGsMoKBGjjA5_GkJt5Oyu_tMzxmijZmXIMCyO-sEbXmYuMOO0W9xNTi04dy4RmXCSLcUCeeHfBVKPBmz7byRNO4zQc8i3YoTW4ihptqCmmWO4wQmm9LpZSb8i-FKakpaR5g-EXfKK7xGpCYGZh_rm6f0_kOp9_2X_bbtyqBrrDC839grW7c81QUA';
