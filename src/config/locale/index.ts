import chat from './chat.json';

export const languages = [
    { name: "English", code: "en" },
    { name: "Luxembourgish", code: "lb" }
];

export const locale = {
    chat
};
