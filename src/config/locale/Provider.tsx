import React, { FC } from 'react';
import { LocalizeProvider } from 'react-localize-redux';
import ReactDOMServer from 'react-dom/server';

import { languages, locale } from './';

const LocalizeProviderWrapper: FC = ({ children }) => {
    const localStorageState = localStorage.getItem('state');
    let lang = languages[0].code;
    if (localStorageState) {
        const localStorageStateParse = JSON.parse(localStorageState);
        if (localStorageStateParse.language) {
            lang = localStorageStateParse.language;
        }
    }

    return (
        <LocalizeProvider
            initialize={{
                languages,
                translation: locale,
                options: {
                    defaultLanguage: lang,
                    renderToStaticMarkup: ReactDOMServer.renderToStaticMarkup
                }
            }}
        >
            {children}
        </LocalizeProvider>
    );
}

export default LocalizeProviderWrapper;
