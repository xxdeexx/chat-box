import { disbatch } from 'redux-act';

import { configureStore } from 'store';

import { 
    SETTINGS_FIELD_CHANGE,
    SETTINGS_FIELD_CHANGE_ACTION,
    SETTINGS_RESET,
    SETTINGS_RESET_ACTION,
    WS_RECEIVE_MESSAGE,
    WS_SEND_MESSAGE,
    WS_ADD_MESSAGE_ACTION,
    settingsFieldChangeSaga,
    settingsResetSaga,
    receiveWsMessageSaga,
    sendWsMessageSaga
} from '../';

describe('app module', () => {
    it("trigger Actions", () => {
        disbatch(
            configureStore(),
            SETTINGS_FIELD_CHANGE({}),
            SETTINGS_FIELD_CHANGE_ACTION({}),
            SETTINGS_RESET(),
            SETTINGS_RESET_ACTION(),
            WS_RECEIVE_MESSAGE({}),
            WS_SEND_MESSAGE({}),
            WS_ADD_MESSAGE_ACTION({
                message: "test 1",
                dateTime: "2020-03-15T12:15:51.968Z",
                isGuest: false,
                userName: ""
            }),
            WS_ADD_MESSAGE_ACTION({
                message: "test 2",
                dateTime: "2020-03-15T12:15:51.968Z",
                isGuest: false,
                userName: ""
            }),
            WS_ADD_MESSAGE_ACTION({
                message: "test 2",
                dateTime: "2020-04-15T12:15:51.968Z",
                isGuest: false,
                userName: ""
            })
        );
    });

    it("settingsFieldChangeSaga", () => {
        const gen = settingsFieldChangeSaga({
            payload: {
                value: 'value test',
                name: 'inputText'
            }
        });
        gen.next();
        gen.next();
        expect(gen.next().done).toBeTruthy();
    })

    it("settingsFieldChangeSaga, set name sendMessageControl", () => {
        const gen = settingsFieldChangeSaga({
            payload: {
                value: 'value test',
                name: 'sendMessageControl'
            }
        });
        gen.next();
        gen.next();
        expect(gen.next().done).toBeTruthy();
    })

    it("settingsResetSaga", () => {
        const gen = settingsResetSaga();
        gen.next();
        expect(gen.next().done).toBeTruthy();
    })

    it("receiveWsMessageSaga", () => {
        const gen = receiveWsMessageSaga({ payload: {} });
        gen.next();
        expect(gen.next().done).toBeTruthy();
    })

    it("sendWsMessageSaga", () => {
        const gen = sendWsMessageSaga({ payload: {} });
        gen.next();
        expect(gen.next().done).toBeTruthy();
    })
});
