import { createSelector } from 'reselect';

import { IAllState } from 'types';

const userNameSelector = (state: IAllState) => state.app.userName;
export const getUserName = createSelector([ userNameSelector ], userName => userName);

const themeColorSelector = (state: IAllState) => state.app.themeColor;
export const getThemeColor = createSelector([ themeColorSelector ], themeColor => themeColor);

const hourDisplaySelector = (state: IAllState) => state.app.hourDisplay;
export const getHourDisplay = createSelector([ hourDisplaySelector ], hourDisplay => hourDisplay);

const sendMessageControlSelector = (state: IAllState) => state.app.sendMessageControl;
export const getSendMessageControl = createSelector([ sendMessageControlSelector ], sendMessageControl => sendMessageControl);

const messagesSelector = (state: IAllState) => state.app.messages;
export const getMessages = createSelector([ messagesSelector ], getMessages => getMessages);
