import { createReducer, createAction } from 'redux-act';
import { put, takeEvery } from 'redux-saga/effects';

import { languages } from 'config/locale';
import { constructMessage } from 'helpers/chat';
import { secondsDifference } from 'helpers/dateTime';

import { IState, THEME_TYPE } from 'types';

export * from './selector';

export const SETTINGS_FIELD_CHANGE = createAction<any>('app/SETTINGS_FIELD_CHANGE');
export const SETTINGS_FIELD_CHANGE_ACTION = createAction<any>('app/SETTINGS_FIELD_CHANGE_ACTION');

export const SETTINGS_RESET = createAction('app/SETTINGS_RESET');
export const SETTINGS_RESET_ACTION = createAction('app/SETTINGS_RESET_ACTION');

export const WS_RECEIVE_MESSAGE = createAction<any>('app/WS_RECEIVE_MESSAGE');
export const WS_SEND_MESSAGE = createAction<any>('app/WS_SEND_MESSAGE');
export const WS_ADD_MESSAGE_ACTION = createAction<any>('app/WS_ADD_MESSAGE_ACTION');

const localStorageState = localStorage.getItem('state');

const defaultState = {
	userName: 'guest001',
	themeColor: THEME_TYPE.LIGHT,
	hourDisplay: 'hh',
	sendMessageControl: false,
	language: languages[0].code,
	messages: []
}

const initialState: IState = localStorageState
	? {
		...JSON.parse(localStorageState)
	}
	: defaultState;

export const reducer = createReducer<IState>({}, initialState);

const storeToLocalStorage = (newState: any) => {
	const serializedState = JSON.stringify({ ...newState, messages: [] });
	localStorage.setItem('state', serializedState);
}

reducer.on(SETTINGS_FIELD_CHANGE_ACTION, (state, payload) => {
	const newState = {
		...state,
		...payload
	}
	storeToLocalStorage(newState);

	return newState;
});

reducer.on(SETTINGS_RESET_ACTION, (state) => {
	const newState = {
		...defaultState,
		isSettingsOpen: true,
		messages: [
			...state.messages
		]
	}
	storeToLocalStorage(newState);

	return newState;
});

reducer.on(WS_ADD_MESSAGE_ACTION, (state, payload) => {
	const messages = [...state.messages];
	const latestMessage = messages[messages.length - 1];
	if (latestMessage) {
		const secondsDif = secondsDifference(payload.dateTime, latestMessage.dateTime);
		if (secondsDif <= 60 && latestMessage.userName === payload.userName) {
			messages[messages.length - 1] = {
				...latestMessage,
				message: `${latestMessage.message}\n${payload.message}`
			};
		} else {
			messages.push(payload)
		}
	} else {
		messages.push(payload);
	}
	return {
		...state,
		messages
	}
});

export function * settingsFieldChangeSaga(data: any) {
	const { payload } = data;
	let value = payload.value;

	switch(payload.name) {
		case "sendMessageControl":
			value = (value === 'true');
			break;
		default:
			value = String(value);
	}

	yield put(SETTINGS_FIELD_CHANGE_ACTION({
		[payload.name]: value
	}));
}

export function * settingsResetSaga() {
	yield put(SETTINGS_RESET_ACTION());
}

export function * receiveWsMessageSaga(action: any) {
	yield put(WS_ADD_MESSAGE_ACTION(action.payload));
}

export function * sendWsMessageSaga(action: any) {
	yield put(WS_ADD_MESSAGE_ACTION(constructMessage(action.payload, false)));
}

export const rootSaga = function * () {
	yield takeEvery(SETTINGS_FIELD_CHANGE, settingsFieldChangeSaga);
	yield takeEvery(SETTINGS_RESET, settingsResetSaga);
	yield takeEvery(WS_RECEIVE_MESSAGE, receiveWsMessageSaga);
	yield takeEvery(WS_SEND_MESSAGE, sendWsMessageSaga);
}
