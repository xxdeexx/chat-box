import { combineReducers } from 'redux';

import * as App from './app';

export const createRootReducer = () => combineReducers({
	app: App.reducer
})

export const rootSagas = [
	App.rootSaga
]
