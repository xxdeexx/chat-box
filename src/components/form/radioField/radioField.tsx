import React, { FC, Fragment } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';

import { ICommonFieldProps } from 'types';

export const RadioFieldInput: FC<ICommonFieldProps> = ({
    name,
    id,
    label,
    value = '',
    callback,
    options = []
}) => {
    const [ inputValue, setInputValue ] = React.useState(String(value));

    React.useEffect(() => {
        setInputValue(String(value));
    }, [value]);

    const onChange = (event: any) => {
        setInputValue(event.target.value);
        if (callback) {
            callback({
                name,
                value: event.target.value
            });
        }
    }

    return (
        <Fragment>
            <FormLabel component="legend">{label}</FormLabel>
            <RadioGroup
                aria-label="position"
                name={id}
                value={inputValue}
                onChange={onChange}
                row
            >
                {options.map((option, key) => (
                    <FormControlLabel
                        key={`radio${id}${key}`}
                        value={String(option.value)}
                        label={option.label}
                        labelPlacement="end"
                        control={<Radio color="primary" />}
                    />
                ))}
            </RadioGroup>
        </Fragment>
    )
};
