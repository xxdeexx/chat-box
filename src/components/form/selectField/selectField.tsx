import React, { FC, Fragment } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { ICommonFieldProps } from 'types';

export const SelectFieldInput: FC<ICommonFieldProps> = ({
    name,
    id,
    label,
    value = '',
    callback,
    options = []
}) => {
    const onChange = (event: any) => {
        if (callback) {
            callback({
                name,
                value: event.target.value
            });
        }
    }

    return (
        <Fragment>
            <InputLabel htmlFor={id}>{label}</InputLabel>
            <Select
                value={value}
                onChange={onChange}
                labelWidth={75}
                inputProps={{
                    name: id,
                    id: id,
                }}
            >
                {options.map((option, key) => (
                    <MenuItem key={`lang${key}`} value={option.value}>{option.label}</MenuItem>
                ))}
            </Select>
        </Fragment>
    )
};
