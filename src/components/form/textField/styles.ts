import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
    inputText: {
        width: '100%',
    }
}));

export default useStyles;
