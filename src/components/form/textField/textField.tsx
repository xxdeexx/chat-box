import React, { FC } from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

import { ITextFieldProps } from 'types';

import useStyles from './styles';

export const TextFieldInput: FC<ITextFieldProps> = ({
    name,
    id,
    label,
    value = '',
    callback,
    callbackOnSubmit,
    callbackOnCtrlSubmit,
    callbackOnBlur,
    endAdornment,
    ...otherProps
}) => {
    const classes = useStyles();

    const [ inputValue, setInputValue ] = React.useState(value);

    React.useEffect(() => {
        setInputValue(value);
    }, [value]);

    const onCallback = (leaveEmpty?: boolean) => {
        if (callback && inputValue) {
            callback({
                name,
                value: inputValue
            });
            if (leaveEmpty) {
                setInputValue('');
            }
        }
    }

    const onBlur = () => {
        if (callbackOnBlur) {
            onCallback();
        }
    }

    const onKeyPress = (event: any) => {
        if ((callbackOnSubmit && event.key === 'Enter') || (callbackOnCtrlSubmit && event.key === 'Enter' && event.ctrlKey)) {
            event.preventDefault();
            onCallback(true);
        }
    }

    const onChange = (event: any) => {
        setInputValue(event.target.value);
    }

    return (
        <TextField
            id={id}
            type="text"
            variant="outlined"
            value={inputValue}
            className={classes.inputText}
            onBlur={onBlur}
            onChange={onChange}
            onKeyPress={onKeyPress}
            {...label ? { label } : {}}
            InputProps={endAdornment ? {
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                onClick={() => onCallback(true)}
                            >
                                {endAdornment}
                            </IconButton>
                        </InputAdornment> 
                    )
                } : {}
            }
            {...otherProps}
        />
    )
};
