import React, { FC } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Settings from '@material-ui/icons/Settings';

import { Translate } from 'react-localize-redux';
import { translateChat } from 'helpers/translate';

import { IHeaderProps } from 'types';

import useStyles from './styles';

export const Header: FC<IHeaderProps> = ({ toggleSettings }) => {
    const classes = useStyles();

    return (
        <AppBar position="static" className={classes.headerWrapper}>
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    <Translate id={translateChat('title')} />
                </Typography>
                <IconButton
                    color="inherit"
                    onClick={toggleSettings}
                >
                    <Settings />
                </IconButton>
            </Toolbar>
        </AppBar>
    )
};
