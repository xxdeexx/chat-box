import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    headerWrapper: {
        marginBottom: `${theme.spacing(2)}px`,
    },
    title: {
        flexGrow: 1,
    }
}));

export default useStyles;
