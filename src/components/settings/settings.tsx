import React, { FC, Fragment } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';

import { CALLBACK, ISettingsProps, THEME_TYPE } from 'types';

import { Translate } from 'react-localize-redux';
import { translateChat } from 'helpers/translate';

import TextFieldInput from 'components/form/textField';
import SelectFieldInput from 'components/form/selectField';
import RadioFieldInput from 'components/form/radioField';

import useStyles from './styles';

export const Settings: FC<ISettingsProps> = ({
    userName,
    themeColor,
    hourDisplay,
    sendMessageControl,
    languages,
    activeLanguage,
    setActiveLanguage,
    settingsFieldChangeAction,
    settingsResetAction
}) => {
    const classes = useStyles();

    const callback = (payload: CALLBACK) => {
        settingsFieldChangeAction(payload);
    }

    return (
        <div className={classes.settingsWrapper}>
            <FormControl
                className={classes.formControl}
            >
                <TextFieldInput
                    name="userName"
                    id="text-user-name"
                    label={<Translate id={translateChat('userName')} />}
                    value={userName}
                    callback={callback}
                    callbackOnBlur
                />
            </FormControl>
    
            <FormControl
                className={classes.formControl}
                component="fieldset"
            >
                <RadioFieldInput
                    name="themeColor"
                    id="radio-interface-color"
                    label={<Translate id={translateChat('interfaceColor')} />}
                    value={themeColor}
                    options={[
                        { value: THEME_TYPE.LIGHT, label: 'Light' },
                        { value: THEME_TYPE.DARK, label: 'Dark' }
                    ]}
                    callback={callback}
                />
            </FormControl>
    
            <FormControl
                className={classes.formControl}
                component="fieldset"
            >
                <RadioFieldInput
                    name="hourDisplay"
                    id="radio-clock-display"
                    label={<Translate id={translateChat('clockDisplay')} />}
                    value={hourDisplay}
                    options={[
                        { value: 'hh', label: <Fragment>12 <Translate id={translateChat('hours')} /></Fragment> },
                        { value: 'HH', label: <Fragment>24 <Translate id={translateChat('hours')} /></Fragment> }
                    ]}
                    callback={callback}
                />
            </FormControl>
    
            <FormControl
                className={classes.formControl}
                component="fieldset"
            >
                <RadioFieldInput
                    name="sendMessageControl"
                    id="radio-send-message-control"
                    label={<Translate id={translateChat('sendMessageControl')} />}
                    value={String(sendMessageControl)}
                    options={[
                        { value: true, label: <Translate id={translateChat('on')} /> },
                        { value: false, label: <Translate id={translateChat('off')} /> }
                    ]}
                    callback={callback}
                />
            </FormControl>

            <FormControl variant="outlined" className={classes.formControl}>
                <SelectFieldInput
                    name="language"
                    id="select-language"
                    label={<Translate id={translateChat('language')} />}
                    value={activeLanguage.code}
                    options={languages.map(lang => ({
                        value: lang.code,
                        label: lang.name
                    }))}
                    callback={(payload: CALLBACK) => {
                        setActiveLanguage(String(payload.value));
                        callback(payload);
                    }}
                />
            </FormControl>

            <FormControl variant="outlined" className={classes.buttonWrapper}>
                <Button
                    variant="outlined"
                    color="secondary"
                    size="large"
                    onClick={() => {
                        setActiveLanguage(languages[0].code);
                        settingsResetAction();
                    }}
                >
                    <Translate id={translateChat('resetSettings')} />
                </Button>
            </FormControl>
        </div>
    )
};
