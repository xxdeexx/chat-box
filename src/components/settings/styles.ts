import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    settingsWrapper: {
        padding: `${theme.spacing(3)}px ${theme.spacing(2)}px`,
    },
    formControl: {
        width: '100%',
        marginBottom: `${theme.spacing(3)}px`,
    },
    buttonWrapper: {
        width: '100%',
        position: 'absolute',
        left: 0,
        bottom: 0,
        padding: `${theme.spacing(2)}px`,
    }
}));

export default useStyles;
