import { connect } from 'react-redux';
import { withLocalize } from "react-localize-redux";

import { IAllState } from 'types';

import {
	getUserName,
    getThemeColor,
    getHourDisplay,
    getSendMessageControl,
    SETTINGS_FIELD_CHANGE,
    SETTINGS_RESET
} from 'modules/app';

import { Settings } from './settings';

const mapStateToProps = (state: IAllState) => ({
    userName: getUserName(state),
    themeColor: getThemeColor(state),
    hourDisplay: getHourDisplay(state),
    sendMessageControl: getSendMessageControl(state)
})

const mapDispatchToProps = {
    settingsFieldChangeAction: SETTINGS_FIELD_CHANGE,
    settingsResetAction: SETTINGS_RESET
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withLocalize(Settings));
