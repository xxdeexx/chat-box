import { connect } from 'react-redux';

import { IAllState } from 'types';

import {
	getMessages
} from 'modules/app';

import { ChatHistory } from './chatHistory';

const mapStateToProps = (state: IAllState) => ({
    messages: getMessages(state)
})

export default connect(
    mapStateToProps
)(ChatHistory);

