import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    chatHistoryWrapper: {
        height: `calc(100vh - ${theme.spacing(19)}px)`,
        marginBottom: `${theme.spacing(1)}px`,
        padding: `0 ${theme.spacing(1)}px`,
        overflow: 'auto',
        [theme.breakpoints.down('xs')]: {
            height: `calc(100vh - ${theme.spacing(18)}px)`,
        },
    }
}));

export default useStyles;
