import React, { FC } from 'react';

import { IState } from 'types';

import Message from './message';

import useStyles from './styles';

export const ChatHistory: FC<Pick<IState, 'messages'>> = ({ messages }) => {
    const classes = useStyles();

    return (
        <div className={classes.chatHistoryWrapper}>
            {messages.map((message, key) => 
                <Message
                    key={`messageList${key}`}
                    userName={message.userName}
                    dateTime={new Date(message.dateTime)}
                    isGuest={message.isGuest}
                >
                    {message.message.split('\n').map((msg: string, key: number) => <span key={`msg${key}`}>{msg}</span>)}
                </Message>
            )}
        </div>
    )
};
