import React, { FC } from 'react';
import Moment from 'react-moment';
import AccountBoxIcon from '@material-ui/icons/AccountBox';

import { formatDateTime } from 'helpers/dateTime';
import Linkify from 'react-linkify';

import useStyles from './styles';

interface IProps {
    isGuest?: boolean;
    userName?: string;
    dateTime?: any;
    hourDisplay: string;
}

export const Message: FC<IProps> = ({ hourDisplay, isGuest, children, userName, dateTime }) => {
    const classes = useStyles({ isGuest });

    return (
        <div className={classes.chatMessageWrapper}>
            {isGuest && <div className={classes.icon}><AccountBoxIcon fontSize="large" /></div>}
            <div
                className={classes.message}
            >
                <div className={classes.info}>
                    {isGuest && <span className={classes.infoUsername}>{userName}</span>}
                    <span className={classes.infoDateTime}>
                        <Moment format={formatDateTime(dateTime, hourDisplay)}>{dateTime}</Moment>
                    </span>
                </div>
                <Linkify>{children}</Linkify>
            </div>
        </div>
    )
};
