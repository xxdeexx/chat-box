import { connect } from 'react-redux';

import { IAllState } from 'types';

import {
    getHourDisplay
} from 'modules/app';

import { Message } from './message';

const mapStateToProps = (state: IAllState) => ({
    hourDisplay: getHourDisplay(state)
})

export default connect(
    mapStateToProps
)(Message);


