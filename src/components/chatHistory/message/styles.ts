import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

import { THEME_TYPE } from 'types';

interface IStyleProps {
    isGuest?: boolean;
}

const useStyles = makeStyles<Theme, IStyleProps>((theme: Theme) => ({
    chatMessageWrapper: (props) => ({
        position: 'relative',
        display: 'flex',
        marginBottom: `${theme.spacing(2)}px`,
        justifyContent: props.isGuest ? 'flex-start' : 'flex-end',
    }),
    icon: {
        width: '43px',
        color: theme.palette.type === THEME_TYPE.LIGHT  ? 'rgba(0, 0, 0, 0.87)' : '#FFFFFF',
    },
    message: (props) => ({
        padding: `${theme.spacing(1)}px`,
        borderRadius: '4px',
        fontSize: '0.75rem',
        background: props.isGuest ? '#EDEDED' : '#2C728F',
        color: props.isGuest ? '#000000' : '#FFFFFF',
        '& > span': {
            display: 'block',
            textAlign: props.isGuest ? 'left' : 'right',
        },
        '& a': {
            color: theme.palette.type === THEME_TYPE.LIGHT ? 
                props.isGuest ? 'rgba(0, 0, 0, 0.87)' : '#FFFFFF' :
                    props.isGuest ? 'rgba(0, 0, 0, 0.87)' : '#FFFFFF',
        },
    }),
    info: {
        marginBottom: `${theme.spacing(.5)}px`,
        display: 'flex',
    },
    infoUsername: {
        fontWeight: 'bold',
        paddingRight: `${theme.spacing(1)}px`,
    },
    infoDateTime: (props) => ({
        flexGrow: 1,
        textAlign: props.isGuest ? 'right' : 'left',
        fontStyle: 'italic',
        fontSize: '0.6rem',
    })
}));

export default useStyles;
