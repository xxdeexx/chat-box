import { connect } from 'react-redux';

import { IAllState } from 'types';

import {
    getSendMessageControl,
    WS_SEND_MESSAGE
} from 'modules/app';

import { ChatInput } from './chatInput';

const mapStateToProps = (state: IAllState) => ({
    sendMessageControl: getSendMessageControl(state)
})

const mapDispatchToProps = {
    sendWsMessageAction: WS_SEND_MESSAGE
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatInput);

