import React, { FC } from 'react';
import FormControl from '@material-ui/core/FormControl';
import SendIcon from '@material-ui/icons/Send';

import TextFieldInput from 'components/form/textField';

import { CALLBACK, IChatInputsProps } from 'types';

import useStyles from './styles';

export const ChatInput: FC<IChatInputsProps> = ({ sendMessageControl, sendWsMessageAction }) => {
    const classes = useStyles();

    const callback = (payload: CALLBACK) => {
        sendWsMessageAction(payload);
    }

    return (
        <div className={classes.chatFormWrapper}>
            <FormControl
                className={classes.formControl}
            >
                <TextFieldInput
                    name="chatInput"
                    id="text-chat"
                    callback={callback}
                    {...sendMessageControl ? { callbackOnCtrlSubmit: true } : { callbackOnSubmit: true }}
                    endAdornment={<SendIcon className={classes.endAdornmentIcon} />}
                />
            </FormControl>
        </div>
    )
};
