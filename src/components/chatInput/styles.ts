import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    chatFormWrapper: {
        height: `${theme.spacing(9)}px`,
        margin: `0 ${theme.spacing(1)}px`,
        borderRadius: '4px',
    },
    formControl: {
        width: '100%',
    },
    endAdornmentIcon: {
        transform: "rotate(-30deg)",
    }
}));

export default useStyles;
