import React from 'react';
import { createStore, compose, applyMiddleware, MiddlewareAPI } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { TypeSocket } from 'typesocket';

import { constructMessage } from 'helpers/chat';

import { createRootReducer, rootSagas } from './modules';
import { WS_RECEIVE_MESSAGE } from './modules/app';

import { CHAT_TOKEN } from './config';

declare global {
    interface Window { INITIAL_REDUX_STATE: any }
}

const socketMiddleware = (url: string) => {
    return (store: MiddlewareAPI<any, any>) => {
        const socket = new TypeSocket<any>(url);
        socket.on('message', (message) => store.dispatch(WS_RECEIVE_MESSAGE(message)));
        socket.connect();

        return (next: (action: any) => void) => (action: any) => {
            if (action.type && action.type.search('app/WS_SEND_MESSAGE') >= 0 && socket.readyState === 1) {
                socket.send(constructMessage({...action.payload, userName: store.getState().app.userName}, true));
            }

            return next(action);
        };
    };
};

const sagaMiddleware = createSagaMiddleware();

export const configureStore = (preloadedState?: any) => {
    const composeEnhancer: typeof compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        createRootReducer(),
        preloadedState,
        composeEnhancer(
            applyMiddleware(
                socketMiddleware(`wss://connect.websocket.in/v2/1986?token=${CHAT_TOKEN}`),
                sagaMiddleware
            ),
        ),
    )

    rootSagas.forEach(sagaMiddleware.run);
  
    return store
}

const ReduxProvider = ({ children, store }: any) => <Provider store={store || configureStore()}>{children}</Provider>;

export default ReduxProvider;
