import { createMuiTheme, makeStyles, Theme } from '@material-ui/core/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export const useStyles = makeStyles((theme: Theme) => ({
    '@global': {
        'body': {
            padding: 0,
            margin: 0,
            background: '#f2f2f2',
			[theme.breakpoints.down('sm')]: {
				padding: 0
			}
		}
    }
}));

const muiTheme = (theme: ThemeOptions) => createMuiTheme(theme);

export default muiTheme;
