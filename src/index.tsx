import React, { FC } from 'react';
import ReactDOM from 'react-dom';

import * as serviceWorker from 'serviceWorker';
import ReduxProvider from 'store';
import LocalizeProvider from 'config/locale/Provider';

import App from 'containers/app';

interface IProps {
    store?: any
}

export const WrapperProvider: FC<IProps> = ({ children, store }) => (
    <ReduxProvider store={store}>
        <LocalizeProvider>
            {children}
        </LocalizeProvider>
    </ReduxProvider>
)

const renderToDOM = () => {
    if (document.getElementById('root')) {
        ReactDOM.render(
            <WrapperProvider>
                <App />
            </WrapperProvider>,
            document.getElementById('root')
        );
    }
}

renderToDOM();

export { renderToDOM };

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
