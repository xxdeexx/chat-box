import React, { FC, Fragment, useEffect } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { translateChat } from 'helpers/translate';

import { IAppProps } from 'types';

import theme, { useStyles } from 'themes';
import Chat from 'containers/chat';

export const App: FC<IAppProps> = ({ themeColor, translate }) => {
	useStyles();

	useEffect(() => {
		document.title = String(translate(translateChat('pageTitle')));
	});

	return (
		<Fragment>
			<CssBaseline />
			<MuiThemeProvider theme={theme({
				palette: {
					type: themeColor
				}
			})}>
				<Chat />
			</MuiThemeProvider>
		</Fragment>
	);
}
