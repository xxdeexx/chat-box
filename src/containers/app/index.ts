import { connect } from 'react-redux';
import { withLocalize } from "react-localize-redux";

import { IAllState } from 'types';

import {
	getThemeColor
} from 'modules/app';

import { App } from './app';

const mapStateToProps = (state: IAllState) => ({
    themeColor: getThemeColor(state)
})

export default connect(
    mapStateToProps
)(withLocalize(App));
