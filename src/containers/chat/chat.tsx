import React, { FC } from 'react';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Typography from '@material-ui/core/Typography';

import { Translate } from 'react-localize-redux';
import { translateChat } from 'helpers/translate';

import Header from 'components/header';
import ChatHistory from 'components/chatHistory';
import ChatInput from 'components/chatInput';
import Settings from 'components/settings';

import useStyles from './styles';

export const Chat: FC = () => {
    const [ settings, setSettings ] = React.useState(false);
    const classes = useStyles();

    const toggleSettings = () => {
        setSettings(!settings);
    }

    return (
        <div className={classes.chatWrapper}>
            <Header toggleSettings={toggleSettings} />
            <ChatHistory />
            <ChatInput />

            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="right"
                open={settings}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <Typography variant="h6" className={classes.title}>
                        <Translate id={translateChat('settings')} />
                    </Typography>
                    <IconButton onClick={toggleSettings}>
                        <ChevronRightIcon />
                    </IconButton>
                </div>
                <Settings />
            </Drawer>
        </div>
    )
};
