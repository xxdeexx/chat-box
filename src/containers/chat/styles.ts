import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

import { THEME_TYPE } from 'types';

const CHAT_WRAPPER_MAX_WIDTH = '500px';

const useStyles = makeStyles((theme: Theme) => ({
    chatWrapper: {
        margin: '0 auto',
        maxWidth: CHAT_WRAPPER_MAX_WIDTH,
        height: `100vh`,
        position: 'relative',
        background: theme.palette.type === THEME_TYPE.LIGHT ? '#FFFFFF' : '#424242',
        overflow: 'hidden',
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        },
    },
    drawer: {
        width: CHAT_WRAPPER_MAX_WIDTH,
        flexShrink: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        [theme.breakpoints.down('xs')]: {
            width: '100%'
        },
    },
    drawerPaper: {
        width: CHAT_WRAPPER_MAX_WIDTH,
        position: 'absolute',
        height: '100vh', 
        border: 'none',
        [theme.breakpoints.down('xs')]: {
            width: '100%'
        },
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 3),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
        borderBottom: '1px solid #dddddd',
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(0, 2),
        },
    },
    title: {
        flexGrow: 1,
    }
}));

export default useStyles;
