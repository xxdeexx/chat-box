# FRONTEND TEMPLATE

Installation
===

One time installation
---
- Install docker - https://store.docker.com/search?type=edition&offering=community
- Install docker-compose
    - On Mac
        - install [homebrew](https://brew.sh/)
        - install docker-compose `brew install docker-compose`

## Running to local
- be in the root folder of repo
- install dependencies `yarn install`
- run `yarn start`

## Running the containers
- be in the root folder of repo
- run `docker-compose up -d --build` to build image and run container
- run `docker-compose -f docker-compose-prod.yml up -d --build` for production build<br><br>

Runs the app in the development mode.<br>
Open [https://localhost:3000](https://localhost:3000) to view it in the browser.

## Stop the containers
- run `docker-compose stop|down`

## Run Test
- run `yarn test` (run changed file test case)
- run `yarn test -- <path>` (run specific test case path)
- run `yarn test -u --watchAll` (run all test case and update snapshot)

## Run EsLint
- run `yarn lint`

## Web Socket Service
- For websocket implementation I used [https://www.websocket.in/](https://www.websocket.in/)
